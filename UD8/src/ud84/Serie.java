package ud84;

public class Serie {
	
	//Atributos
	private String titulo;
	private int temporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	//default
	final boolean defEntregado=false;	
	final int defTemp = 3;
	
	
	// Un constructor por defecto
	public Serie() {
		this.titulo="";
		this.temporadas=defTemp;
		this.entregado=defEntregado;
		this.genero="";
		this.creador="";
	}

	// Un constructor con el titulo y creador. El resto por defecto.

	public Serie(String titulo, String creador) {
		super();
		this.titulo = titulo;
		this.temporadas=defTemp;
		this.entregado=defEntregado;
		this.genero = "";
		this.creador = creador;
	}

	// Un constructor con todos los atributos, excepto de entregado
	
	public Serie(String titulo, int temporadas, String genero, String creador) {
		super();
		this.titulo = titulo;
		this.temporadas = temporadas;
		this.genero = genero;
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", temporadas=" + temporadas + ", entregado=" + entregado + ", genero="
				+ genero + ", creador=" + creador + "]";
	}
	
	
	

	
}
