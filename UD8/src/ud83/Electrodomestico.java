package ud83;

public class Electrodomestico {
	
	//Atributos
	protected int precioBase;
	protected String color;
	protected char consumo;
	protected double peso;
	
	//dlft cons
	final String defColor = "Blanco";
	final int defPrecio = 100;
	final char defConsumo = 'F';
	final int defPeso = 5;
	
	// Un constructor por defecto.

		public Electrodomestico() {
			// TODO Auto-generated constructor stub
			this.precioBase=defPrecio;
			this.color=defColor;
			this.consumo=defConsumo;
			this.peso=defPeso;
		}
		
	//Un constructor con el precio y peso. El resto por defecto.
		
		public Electrodomestico(int precioBase, double peso) {
			super();
			this.precioBase = precioBase;
			this.peso = peso;
			this.color=defColor;
			this.consumo=defConsumo;

		}
		
	//  Un constructor con todos los atributos.	
		public Electrodomestico(int precioBase, String color, char consumo, double peso) {
			super();
			this.precioBase = precioBase;
			this.color = elegirColor(color);
			this.consumo = elegirEner(consumo);
			this.peso = peso;
					
		}
		
		private String  elegirColor(String elcolor) {
				elcolor = (String) elcolor.toLowerCase();
	
			switch (elcolor) {
			case "blanco":
				return "Blanco" ;
			case "negro": 
				return "Negro" ;
			case "rojo":	
				return "Rojo" ;
			case "azul":
				return "Azul" ;
			case "gris":			
				return "Gris" ;

			default: System.out.println("El color por defecto es Blanco, por favor introduzca un color correcto ");
				break;
			}
			return "Blanco";
		}
			
			private char  elegirEner(char elEner) {
				switch (elEner) {
				case 'A':
					return 'A';
				case 'B':
					return 'B';
				case 'C':
					return 'C';
				case 'D':
					return 'D';
				case 'E':
					return 'E';
				case 'F':
					return 'F';
			

				default: System.out.println("El consumo por defecto es F, por favor seleccione un consumo energetico correcto");
					break;
				}
				return 'F';
			}

			@Override
			public String toString() {
				return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumo=" + consumo
						+ ", peso=" + peso + "kg" + "]";
			}

			
	

}
