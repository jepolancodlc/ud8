package ud82;

import java.util.Random;

public class password {
	
	private String contra;
	private int longitud; 
	
	// Un constructor por defecto.
	
		
		public password() {
			// TODO Auto-generated constructor stub
			this.longitud=8;
			this.contra=getContra();
		}
		
	//Un constructor con la longitud que nosotros le pasemos. Generara un contrase�a aleatoria con esa longitud.
	
		public password(int longitud) {
			this.longitud = longitud;
			this.contra = getContra();
	
		}
	
		private String getContra() {
			// TODO Auto-generated method stub
			Random random = new Random();
			String pwd= "";
			String tecla ="1qazxsw23edcvfr45tgbnhy67ujmki89ol�p0QAZXSWEDCVFRT";
			for (int i = 0; i < longitud; i++) {
				pwd +=tecla.charAt(random.nextInt(tecla.length()));
			}
			return pwd;
		}
	
		@Override
		public String toString() {
			return "password [contra=" + contra + ", longitud=" + longitud + "]";
		}
		
		
	
	
}
