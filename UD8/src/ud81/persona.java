package ud81;

public class persona {
	
	final char genero='H';
	//Atributos
	
	private String nombre;
	private int edad;
	private int dni;
	private char sexo;
	private double peso;
	private double altura;
	
	
	//Constuctor por defecto	
	public persona () {
		this.nombre="";
		this.edad=0;
		this.dni=496468265;
		this.sexo=genero;
		this.peso=0;
		this.altura=0;
			}

		 //Un constructor con el nombre, edad y sexo, el resto por defecto.
	public persona (String nombre, int edad, char sexo) {
			this.nombre=nombre;
			this.edad=edad;
			this.sexo=sexo;
			this.peso=0;
			this.altura=0;
				}
		
		//
		
	public persona (String nombre, int edad, char sexo, int dni, int peso, double altura) {
			this.nombre=nombre;
			this.edad=edad;
			this.dni = dni;
			this.sexo=sexo;
			this.peso=peso;
			this.altura=altura;
		}

	@Override
	public String toString() {
		return "persona [genero=" + genero + ", nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", sexo=" + sexo
				+ ", peso en kg =" + peso + ", altura=" + altura + "]";
	}
		
		
		
		
		
	
}
